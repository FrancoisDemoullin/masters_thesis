%% The following is a directive for TeXShop to indicate the main file
%%!TEX root = diss.tex

\section{Ray-Traced Ambient Occlusion Benchmark}
\label{sec:ao_benchmark_intro}

Ambient Occlusion is omnipresent in virtually all rendering engines. Rasterization based engines like Unreal or Unity include multiple rasterization techniques. Fully-fledged open-source CPU ray-tracers such as PBRT~\cite{Pharr:2010:PBR:1854996} and Mitsuba~\cite{0.1145/3355089.3356498} include built-in support for ray-traced ambient occlusion.

Unfortunately, to the best of our knowledge, no open-source GPU raytracer provides an Ambient Occlusion implementation for academic usage. Unreal Engine provides limited support for ray-traced ambient occlusion with tight integration into Microsoft's real-time DXR API. DXR is closed-source and does not provide the flexibility to be used in academia. 

The lack of open source ambient occlusion GPU benchmarks in academia has driven us to implement our benchmark for use in academia. 
We design a benchmark that allows the user to load any model and to generate the ray-traced ambient occlusion for that model on the GPU using 3 distance GPU raytracers. The user is given the freedom to adjust the most important parameters such as SPP, ray per triangle count, maximum ray count and ray extent.

\subsection{Determining Ray-Origin}
\label{subsec:ray_orig}

The first step in generating ambient occlusion rays for RTAO is to load a model. We adopt a STY model loading format allowing us to load triangular meshes. Our model loader requires vertex positions, connectivity, and vertex normals. 

From the triangular mesh, we then have to generate a shape distribution to reconstruct the model. Although it would be easy to simply use the vertex positions of the mesh as the origins for the AO rays, this would lead to a sub-par ambient occlusion effect ass illustrated in~\autoref{fig:teapot_vertices}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\linewidth]{teapot_vertices}
 
  \caption{\label{fig:teapot_vertices}Using the vertex positions as the origin of the rays for the Teapot model. The regular pattern provides poor surface coverage resulting in a poor visual ambient occlusion effect. Each red dot represents the origin of one ray.}
\end{figure}

We achieve better surface coverage by randomly sampling each triangle primitive of the model.
For each triangle with vertices (A, B, C) we generate two random numbers, $r_{1}$ and $r_{2}$ between 0 and 1 and generate a new point P within the triangle by applying the equation given in~\autoref{eq:p_gen_eq}. For the derivation of ~\autoref{eq:p_gen_eq} refer to~\cite{10.1145/571647.571648}.
Similarily, we generate the normal N at point P using the vertex normals ($A_{N}$, $B_{N}$, $C_{N}$) as shown in~\autoref{eq:n_gen_eq}

\begin{equation}
\label{eq:p_gen_eq}
  P = (1 - \sqrt{r_{1}})A + \sqrt{r_{1}}(1-r_{2})B + \sqrt(r_{1})r_{2}C
\end{equation}
\begin{equation}
\label{eq:n_gen_eq}
  N = (1 - \sqrt{r_{1}})A_{N} + \sqrt{r_{1}}(1-r_{2})B_{N} + \sqrt(r_{1})r_{2}C_{N}
\end{equation}

The tightness of the coverage of origin points depends on the samples per triangle (SPT). A higher number of samples results in a tighter surface cover and a larger number of rays. The tradeoff between a higher SPT is shown in figure~\autoref{fig:spt_tradeoff}.

\begin{figure}[htb]
  \begin{subfigure}{0.32\textwidth}
  \includegraphics[width=\linewidth]{4_spt.png}
  \caption{4 SPT}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.32\textwidth}
  \includegraphics[width=\linewidth]{8_spt.png}
  \caption{8 SPT}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.32\textwidth}
  \includegraphics[width=\linewidth]{16_spt.png}
  \caption{16 SPT}
  \end{subfigure}
  \caption{\label{fig:spt_tradeoff}
           Illustration of samples per triangle (SPT) surface coverage for 4spt, 8spt, and 16spt. Each red dot represents the origin of one ray.
           }
\end{figure}

\subsection{Determining Ray-Direction}
Once the ray-origins have been established~\autoref{subsec:ray_orig} the ray's direction has to be determined.

It is possible to take the normalized surface normal N from~\autoref{eq:n_gen_eq} as the ray direction. This would yield an incomplete coverage of the hemisphere originating at P as illustrated in~\autoref{fig:normal_pos_trivial}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\linewidth]{normal_pos}
 
  \caption{\label{fig:normal_pos_trivial}Using the vertex normal as ray-direction yields poor sampling of the hemisphere originating at P. Each green dot represents the destination of the ray direction of one ray.}
\end{figure}

To compute ray-direction accurately we stochastically sample the hemisphere by using a cosine-weighted distribution function~\cite{10.5555/1196364}.
We first generate an orthonormal basis to the normal N~\cite{Duff2017BuildingAO}. We then stochastically generate a tangent vector using the cosine weighted hemisphere distribution. Finally, we translate the tangent vector into the normal space using the orthonormal basis. The pseudo-code in~\autoref{lst:ray_dir} illustrates the algorithm.

Ture ambient occlusion requires multiple independent stochastical samples from the same origin. From one origin point on the surface model, multiple rays should be shot into distinct directions according to the cosine weighted hemisphere distribution. The number of rays is given by the samples per pixel (SPP). The usage of SPP is illustrated in line 7 in~\autoref{lst:ray_dir}.

\begin{lstlisting}[caption={Generation of the ray direction based on ray normal N},label={lst:ray_dir}, language=C++]
for ray with normal N

  // get orthonormal basis of the normal
  float3 b1, b2
  GetTangentBasis(N, b1, b2);
  
  for i in range(0, SPP) 
  {
    // sample the hemisphere, get a tangent
    float3 tangent = CosineSampleHemisphere();

    // translate tangent into world_normal space
    // tangent * float3x3(row0 = b1, row1 = b2, row2 = n)
    float4 ray_direction = tangent * to_matrix(b1, b2, n);
  }
}
\end{lstlisting}

Figure~\autoref{fig:spp_tradeoff}(a) illustrates the effect of using stochastically distributed ray-directions. The space surrounding the teapot is densely covered. Figure~\autoref{fig:spp_tradeoff}(b) illustrates the effect of doubling SPP to 2, this configuration provides sufficient ray-distribution for high-quality AO.

\begin{figure}[htb]
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[width=\linewidth]{1_spp.png}
  \caption{1 SPP}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[width=\linewidth]{2_spp.png}
  \caption{2 SPP}
  \end{subfigure}
  \caption{\label{fig:spp_tradeoff}
           Illustration of samples per pixel (SPP) using the stochastic hemisphere sampling approach from~\autoref{lst:ray_dir}. Each green dot represents the destination of the ray direction of one ray.
           }
\end{figure}

\subsection{Tracing Rays}

Once all the rays have been generated with origin and directions, they are ready to be traced using existing raytracers. 

We generate the BVH acceleration structure using Intel Embree~\cite{Wald:2014:EKF:2601097.2601199}.
We use 3 existing intersections and trace algorithms.

Aila et al.~\cite{Aila:2009:UER:1572769.1572792} published a raytracing GPU benchmark in 2010. The benchmark uses a binary BVH acceleration structure.
The kernel is implemented as a persistent thread kernel, work is fetched on completion of previous rays. Because this benchmark is a one-ray-per-thread benchmark, a traversal stack is maintained. 
We implement the Aila benchmark in CUDA as a compute kernel, we use CUDA-10 as shown in~\autoref{lst:aila_while_while}.

\begin{lstlisting}[caption={While-While loop for Aila et al.~\cite{Aila:2009:UER:1572769.1572792} raytracing benchmark},label={lst:aila_while_while}, language=C++]
while (ray not terminated)
  while (interior nodes to be intersected)
    intersect interior nodes
    maintain traversal stack

  while (leaf nodes to be intersected)
    intersect primitives
\end{lstlisting}


Ylitie et al.~\cite{Aila:2009:UER:1572769.1572792} published an improvement on the Aila benchmark in 2017.
The improved becnhamrk uses an 8-wide BVH acceleration structure. Using a wide acceleraation structure has the advantage that it shortens the number of interior nodes each ray has to intsersect before reaching a leaf. Ylitie et al. further compress the interior and the leaf nodes, reducing the memory overhead of each traversal. The traversal order for leafs is not fixed, leafs are traversed from front to back according to ray-octant.

We implement the Ylitie benchmark in CUDA as a compute kernel, we use CUDA-10 as shown in~\autoref{lst:ylitie_becnhmark}.


\begin{lstlisting}[mathescape=true, caption={Ylitie et al.~\cite{Aila:2009:UER:1572769.1572792} GPU BVH traversal benchmark},label={lst:ylitie_becnhmark}, language=C++]
r = get_ray() // get ray from ray pool
$S$ = {} // traversal stack
$G$ = {root} // current node to operate on
$G_t$ = {} // current leaf node
while(1)
  /*interior node loop*/
  if $G$ is interior node
    n = get_closest_node(G, r)
    $G$ = $G$ - {n}
    if $G$ is not empty
      S.push(G)
      G = intersect_children(n, r)
  else // G is leaf node
    $G_t$ = $G$    
    $G$ = {}

  /*leaf node loop*/
  while $G_t$ is not empty
    t = get_next_triangle($G_t$)
    $G_t$ = $G_t$ - {t}
    intersect_triangle(t, r)

  /*terminal condition*/
  if G is empty and S is not empty
    G = pop_stack(S)
  else
    return // done!
\end{lstlisting}

Finally, we use NVIDIA's optiX API~\cite{Parker:2010:OGP:1778765.1778803} as a reference. OptiX is the state-of-the art GPU raytracing API. optiX manages it's own acceleration structure and traversal and intersection operation. The closed source nature of OptiX is therefore not suited for academic research. we nonetheless compare our benchmarks for both performance and correctness against OptiX. 

\begin{lstlisting}[mathescape=true, caption={OptiX Prime~\cite{Parker:2010:OGP:1778765.1778803} query implementation},label={lst:optix_becnhmark}, language=C++]
  optix::prime::Context OptiXContext = optix::prime::Context::create(RTP_CONTEXT_TYPE_CUDA);
  optix::prime::Model SceneModel = OptiXContext->createModel();
  SceneModel->setTriangles(rg.IndexBuffer.size(), RTP_BUFFER_TYPE_HOST, rg.IndexBuffer.data(), rg.VertexBuffer.size(), RTP_BUFFER_TYPE_HOST, rg.VertexBuffer.data());
  SceneModel->update(RTP_MODEL_HINT_NONE);
  SceneModel->finish();

  optix::prime::Query query = SceneModel->createQuery(RTP_QUERY_TYPE_CLOSEST);
  query->setRays(numRays, RTP_BUFFER_FORMAT_RAY_ORIGIN_TMIN_DIRECTION_TMAX, RTP_BUFFER_TYPE_CUDA_LINEAR, rg.cudaRays);
  query->setHits(numRays, RTP_BUFFER_FORMAT_HIT_T_TRIID_U_V, RTP_BUFFER_TYPE_CUDA_LINEAR, cudaHits);

  query->execute();
\end{lstlisting}

\section{Visual Ambient Occlusion Output}  

We visualize the ambient occlusion output using the GPU raytraced benchmarks with Blender.
The raytracer outputs a buffer with an entry for each ray, the entry is either 0, indicating that the ray missed all gemoetry, or the entry is a floating point value between $t_{min}$ and $t_{max}$ indicating that the ray hit gemoetry within it's extent.

The output of our ambient occlusion becnhamrk is a .STY file. Each entry in the file corresponds to one 3D point $P$ on the surface of the model. The number of points in file is claculated as:

\begin{equation}
\label{eq:trivial_num_samples}
  NumberOfSamples = NumberOfTriangles * SPT
\end{equation}

Each entry consists furthermore of a grayscale color. The color is representative of the ambient occlusion value. The darker the color the less ambient occlusion is applied. This is counter-intuitive to the natural phenomoenon of ambient occlusion: the more a point is occluded, the darker it should be represented. Our benchmark, however, aims to visualize ambient occlusion independently of all other visual effects. We find that debuggagbility and visual clarity is increased by inversing the grayscale such that a white point represents a perfectly occluded point and a black point represents a little occluded point.
If a point is perfectly unoccluded it is omitted from our visual reprsentation. 

The color of each sample point is determined as shown in~\autoref{lst:color_assignment}.

\begin{lstlisting}[mathescape=true, caption={Assigning color to each sample point}, label={lst:color_assignment}, language=C++]
  for each sample point $P$
    sum = 0
    for each sample in SPP 
      if intersection_distance > t_{min}
        sum++

    grayscale_color = sum / SPP

    if (sum > 0) 
      write $P$ to file
      $P$_{color} = grayscale_color
\end{lstlisting}

In~\autoref{fig:ao_bechmark_complex_scenes} we show the visual output from our raytraced ambient occlusion benchmark on scenes of various complexitiies and triangles counts. All becnhamrks are taken from~\cite{McGuire2017Data}. The summary of the benchmarks is shown in~\autoref{tab:benchmark_description}.

\begin{table}
\begin{tabular}{ |p{3cm}||p{3cm}|p{3cm}|p{3cm}|  }
 \hline
 Scene Name & Number of triangles & BVH Node Memory (MB) & Triangle Node Memory (MB) \\
 \hline
 Teapot     & 15k     & 0.09  & 0.41 \\
 Dragon     & 300k    & 1.6   & 5.7 \\
 Sponza     & 262k    & 4.6   & 17.77 \\
 Buddha     & 1 mio   & ~\note{}  & ~\note{} \\
 Hairball   & 2.8 mio & ~\note{}  & ~\note{} \\
 San Miguel & 7.8 mio & 176   & 574.5 \\
 \hline
\end{tabular}
\caption{\label{tab:benchmark_description}Sample scenes used for performance evalution of our RTAO benchmark}
\end{table}

By using large models such as San-Miguel and Hairball, we show that our benchmark performons on models which are too complex for real-time rendering. 

\begin{figure}[htb]
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[width=\linewidth]{teapot_32_32.png}
  \caption{Utah Teapot}
  \label{subfig:teapot_ao}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{dragon_32_32.png}
  \caption{Dragon}
  \label{subfig:dragon_ao}
  \end{subfigure}
  \qquad
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{sponza_16_16.png}
  \caption{Sponza}
  \label{subfig:sponza_ao}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
   \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{buddha.png}
  \caption{Buddha}
  \label{subfig:buddha_ao}
  \end{subfigure}
  \qquad
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{hairball.png}
  \caption{Hairball}
  \label{subfig:hairball_ao}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{san_miguel.png}
  \caption{San Miguel}
  \label{subfig:san_mig_ao}
  \end{subfigure}
  
  
  \caption{\label{fig:ao_bechmark_complex_scenes}
           Our Ray-Traced Ambient Occlusion output visualized on 8 scenes. We used 32 SPP and 32 SPT for (a) and (b) and 4 SPP and 4 SPT for (c) through (f).
           }
\end{figure}

The biggest impact on quality of the AO effect are the values of SPP and SPT. The user can chose the appropriate value, the higher the values chosen the higher the quality of the ambinent occlusion effect. 

As mentioned in~\autoref{ch:Introduction}, there is an inherent tradeoff in Computer Graphics: quality vs speed. While the highest visual output is achieved with high SPPs and SPTs they are not always necessary for the desired output. Performance scales linearly with both SPT and SPP as discuessed in~\autoref{sec:mem_behavior_performance}, therefroe it is crucial to not waste resources.

~\autoref{fig:spp_tradeoff_AO} illustrates the tradeoff of the SPP valus on the tepot scene, SPT is held constatn at a high value of 32. 1 sample per pixel yields in poor ambient occlusion output. With 1 SPP the color algorithm described in~\autoref{lst:color_assignment} outputs a binary value: there either is occlusion or there is none. 32 SPPs on the other end of the spectrum provide a value in the range from 0-32, this value indicates the amount of ambient occlusion and allows for a much more accurate viusal effect. 

\begin{figure}[htb]
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=4cm, width=\linewidth]{teapot_1_32.png}
  \caption{1 SPP}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=4cm, width=\linewidth]{teapot_2_32.png}
  \caption{2 SPP}
  \end{subfigure}
  \qquad
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=4cm, width=\linewidth]{teapot_4_32.png}
  \caption{4 SPP}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=4cm, width=\linewidth]{teapot_8_32.png}
  \caption{8 SPP}
  \end{subfigure}
  \qquad
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=4cm, width=\linewidth]{teapot_16_32.png}
  \caption{16 SPP}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=4cm, width=\linewidth]{teapot_32_32.png}
  \caption{32 SPP}
  \end{subfigure}

  \caption{\label{fig:spp_tradeoff_AO}
           SPP tradeoff for the teapot scene. SPT is fixed at 32.
           }
\end{figure}

~\autoref{fig:spt_tradeoff_AO} illustrates the tradeoff in chosing the SPT value on the dragon scene while keeping the SPP value constant at a high 32.
SPt roughly translates to the coverage of the surafce area of the model. S small SPT number provides great covereage if the model is composed of many small triangles. A small SPT values provides bad coverage for models with large flat traiangles.
We suggest keeping the SPT value low. Small trangles ina  model suggest sharp angles and complex geomtry. These are the areas where the AO effect is most pronounced. Large flat surfaces generally do not have significant AO. 

\begin{figure}[htb]
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{dragon_spt_1.png}
  \caption{1 SPT}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{dragon_spt_2.png}
  \caption{2 SPT}
  \end{subfigure}
  \qquad
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{dragon_spt_4.png}
  \caption{4 SPT}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{dragon_spt_8.png}
  \caption{8 SPT}
  \end{subfigure}
  \qquad
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{dragon_spt_16.png}
  \caption{16 SPT}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[height=6cm, width=\linewidth]{dragon_spt_32.png}
  \caption{32 SPT}
  \end{subfigure}

  \caption{\label{fig:spt_tradeoff_AO}
           SPT tradeoff for the dragon scene. SPP is fixed at 32.
           }
\end{figure}


\section{Performance and Memory behaviour of GPU Ray-Traced Ambient Occlusion} 
\label{sec:mem_behavior_performance} 

\subsection{Performance evaluation}

~\autoref{tab:ao_to_completion} shows the time it takes to render the complete AO effect for our bechmark scenes. The test configuration uss 32 SPP and 1 SPT. The results produced are a high quality AO effect. 
Naturally, it takes longer to trace large scenes. Larger scenes have more traingles and therefore more rays are traced.

Accross benchmarks, a trend can be observed. The Aila et al. benchmark using a binary BVH generally performs slower than Ylitie et al. and optiX prime. OptiX performs best of all our benchmarks. We suspect OptiX uses highly optimized cuda kernels and pre-processes the rays to increas ray-coherence and ray locality. The closed-source nature of OptiX prevents us from confirming our speculations. 

\begin{table}
\begin{tabular}{ |p{3cm}||p{3cm}|p{3cm}|p{3cm}|  }
 \hline
 Scene Name & Aila et al.~\cite{Aila:2009:UER:1572769.1572792} & Ylitie et al.~\cite{Ylitie:2017:EIR:3105762.3105773} & NVIDIA OptiX prime~\cite{Parker:2010:OGP:1778765.1778803} \\
 \hline
 Teapot     & 0.223   & 0.144  & 0.199 \\
 Dragon     & 3.28    & 1.819   & 1.99 \\
 Sponza     & 3.488    & 4.043   & 3.19 \\ 
 San Miguel & 872.3 & 392.6  & 363.57 \\
 Buddha     & 1141.019   & 395.724  & 966.537 \\
 Hairball   & 20.283 & 10.778  & 8.99 \\
 \hline
\end{tabular}
\caption{\label{tab:ao_to_completion}The time (in mS) rendering the complete AO effect at 32 SPP and 1 SPT per scene for the 3 GPU raytracers}
\end{table}

\subsubsection{Effect on scene size on performance}

\subsubsection{Effect on SPP on performance}

\subsubsection{Feasibility on mobile devices}

\subsection{Memory evaluation}

We use GPGPU-Sim~\cite{4919648} to analyze the detalied behaviour of out GPU AO benchmark on modern GPU architectures. 
GPGPU-Sim is a cycle-level simulator that simulates the CUDA and provides detailed statistics.

As a case study, we evaluate the Ylitie et al.~\cite{Ylitie:2017:EIR:3105762.3105773} becnhmark for the dragon and sponze scenes.
We chose the Ylitie et al. becnhamrk over the Aila et al. becnhamrk becuase, in the general case, it performs better. The Ylitie et al. benchmark is the fastest GPU raytracer to date. 

We evaluate the dragon and the sponza scene as they are mid-range scenes in terms of complexity. Evaluationg a small scene results in incorrect results. Raytracing applicaitons are memory bound. Small scenes, however, are compute bound. The reason for this discrepancy is thatfor small scenes the entire accelearion structure and the leaf nodes fit in the L2 cache. This is not a realistic workload, video game scenes and move scnes are much larger than any L2 cache. Both the Sponza and the Dragon scene exceed the L2 chace of conventional GPUs and are therefore appropriate scenes for a memory study. 

\subsubsection{Memory access classification}

~\autoref{fig:LDST_distrib} shows the distribution of the memory accesses. All memory accesses are generated at one of the many GPU cores.
We log these accesses before they enter the Load-Store queue (LDST). when memory accesses are generated, they are not dependend on the cache hirarchy. 
memory accesses are clssified by buffer: a memory access can be assigned to one of 5 buffers:

\begin{enumerate}
   \item BVH interior node buffer - a flattened buffer representing the interior nodes of the BVH tree
   \item leaf node buffer - a flattened buffer representing the triangle information of the scene
   \item ray buffer - each entry represents a ray
   \item ray-result buffer - each entry is the result of the corresponding ray from the ray buffer
   \item traversal stack - the traversal stack is kept in shared memory but can occasionally spill to DRAM
\end{enumerate}



From ~\autoref{fig:LDST_distrib} it is evident that most memory accesses are accessing the interior nodes of the acceleration structure, followed by memory accesses for the triangles of the scene. 

\begin{figure}[htb]
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[width=\linewidth]{dragon_LDST_mem_distrib.png}
  \caption{Dragon}
  \end{subfigure}
  \hspace*{\fill} % separation between the subfigures
  \begin{subfigure}{0.49\textwidth}
  \includegraphics[width=\linewidth]{sponza_LDST_mem_distrib.png}
  \caption{Sponza}
  \end{subfigure}

  \caption{\label{fig:LDST_distrib}
           Distribution of memory accesses generated at the GPU compute cores. 100k rays, 8 SPP, 8SPT.
           }
\end{figure}

\subsubsection{Cache performance}

\subsubsection{Memory reuse}